package com.tktpl.helloworld.tutorial4.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.tktpl.helloworld.R;
import com.tktpl.helloworld.tutorial4.Tutorial4ViewModel;

public class FirstFragment extends Fragment {
    View view;
    Button firstFragmentButton;
    Tutorial4ViewModel viewModel;
    TextView fragmentText;
    final String DATA = "This is passed data from first fragment to second fragment!";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tutorial4_first, container, false);
        firstFragmentButton = (Button) view.findViewById(R.id.firstButton);
        viewModel = ViewModelProviders.of(getActivity()).get(Tutorial4ViewModel.class);
        fragmentText = (TextView) view.findViewById(R.id.fragmentTextFirst);
        fragmentText.setText(viewModel.data);

        firstFragmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.data = DATA;
                Toast.makeText(getActivity(), "Data has been passed to second fragment!", Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }
}
