package com.tktpl.helloworld.tutorial4.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.tktpl.helloworld.R;
import com.tktpl.helloworld.tutorial4.Tutorial4ViewModel;

public class SecondFragment extends Fragment {
    View view;
    Button secondFragmentButton;
    TextView fragmentText;
    Tutorial4ViewModel viewModel;
    final String DATA = "This is passed data from second fragment to first fragment!";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tutorial4_second, container, false);
        secondFragmentButton = (Button) view.findViewById(R.id.secondButton);
        viewModel = ViewModelProviders.of(getActivity()).get(Tutorial4ViewModel.class);
        fragmentText = (TextView) view.findViewById(R.id.fragmentText);
        fragmentText.setText(viewModel.data);

        secondFragmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.data = DATA;
                Toast.makeText(getActivity(), "Data has been passed to first fragment!", Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }
}
