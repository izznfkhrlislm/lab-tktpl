package com.tktpl.helloworld.tutorial4;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.tktpl.helloworld.R;
import com.tktpl.helloworld.tutorial4.fragments.FirstFragment;
import com.tktpl.helloworld.tutorial4.fragments.SecondFragment;

public class Tutorial4Activity extends AppCompatActivity {
    Button firstFragment, secondFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial4);

        firstFragment = (Button) findViewById(R.id.firstFragment);
        secondFragment = (Button) findViewById(R.id.secondFragment);
        if (savedInstanceState == null) {
            launchFragment(new FirstFragment());
        }
    }

    public void launchFragment1(View view) {
        //TODO: load first fragment
        launchFragment(new FirstFragment());
    }

    public void launchFragment2(View v) {
        //TODO: load second fragment
        launchFragment(new SecondFragment());
    }

    private void launchFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}