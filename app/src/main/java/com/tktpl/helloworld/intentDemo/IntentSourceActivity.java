package com.tktpl.helloworld.intentDemo;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import com.tktpl.helloworld.R;

public class IntentSourceActivity extends AppCompatActivity {
    private static final String TAG = "IntentSourceActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intentsource);
    }

    public void passDataSourceButton(View view) {
        Intent intent = new Intent(IntentSourceActivity.this, IntentTargetActivity.class);
        intent.putExtra("message", "This message comes from IntentSourceActivity class");
        startActivity(intent);
    }
}
