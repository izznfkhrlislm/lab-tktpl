package com.tktpl.helloworld.intentDemo;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.tktpl.helloworld.R;

public class IntentTargetActivity extends AppCompatActivity {
    private static final String TAG = "IntentTargetActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intenttarget);

        Intent intent = getIntent();
        String passedMsg = intent.getStringExtra("message");
        TextView text = (TextView) findViewById(R.id.resultData);
        text.setText(passedMsg);
    }

}
