package com.tktpl.helloworld;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import com.tktpl.helloworld.intentDemo.IntentSourceActivity;
import com.tktpl.helloworld.tutorial2.Tutorial2Activity;
import com.tktpl.helloworld.tutorial3.Tutorial3Activity;
import com.tktpl.helloworld.tutorial4.Tutorial4Activity;

public class WelcomeScreenActivity extends AppCompatActivity {
    private static final String TAG = "WelcomeScreenActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcomescreen);
    }

    public void invokeTutorial2(View view) {
        Log.i(TAG, "Tutorial 2 invoked!");
        Intent intent = new Intent(WelcomeScreenActivity.this, Tutorial2Activity.class);
        startActivity(intent);
    }

    public void invokeIntentDemo(View view) {
        Log.i(TAG, "Intent Demo invoked!");
        Intent intent = new Intent(WelcomeScreenActivity.this, IntentSourceActivity.class);
        startActivity(intent);
    }

    public void invokeTutorial3(View view) {
        Log.i(TAG, "Tutorial 2 invoked!");
        Intent intent = new Intent(WelcomeScreenActivity.this, Tutorial3Activity.class);
        startActivity(intent);
    }

    public void invokeTutorial4(View view) {
        Log.i(TAG, "Tutorial 4 invoked!");
        Intent intent = new Intent(WelcomeScreenActivity.this, Tutorial4Activity.class);
        startActivity(intent);
    }
}
