package com.tktpl.helloworld.tutorial2.sharedPreferences;

import android.content.SharedPreferences;

import java.util.Calendar;

public class SharedPreferencesHelper {
    static final String KEY_NAME = "key_name";
    static final String KEY_DOB = "key_dob_millis";
    static final String KEY_EMAIL = "key_email";

    private final SharedPreferences mSharedPreferences;

    public SharedPreferencesHelper(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    public boolean savePersonalInfo(SharedPreferenceEntry sharedPreferenceEntry) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_NAME, sharedPreferenceEntry.getName());
        editor.putLong(KEY_DOB, sharedPreferenceEntry.getDob().getTimeInMillis());
        editor.putString(KEY_EMAIL, sharedPreferenceEntry.getEmail());

        return editor.commit();
    }

    public boolean removePersonalInfo() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_NAME, "");
        editor.putLong(KEY_DOB, 0);
        editor.putString(KEY_EMAIL, "");

        return editor.commit();
    }

    public SharedPreferenceEntry getUserInfo() {
        String name = mSharedPreferences.getString(KEY_NAME, "");
        Long dob = mSharedPreferences.getLong(KEY_DOB, Calendar.getInstance().getTimeInMillis());
        String email = mSharedPreferences.getString(KEY_EMAIL, "");

        Calendar dobObject = Calendar.getInstance();
        dobObject.setTimeInMillis(dob);

        return new SharedPreferenceEntry(name, dobObject, email);
    }
}
