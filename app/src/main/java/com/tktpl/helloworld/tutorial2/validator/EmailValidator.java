package com.tktpl.helloworld.tutorial2.validator;

import android.text.Editable;
import android.text.TextWatcher;

import java.util.regex.Pattern;

public class EmailValidator implements TextWatcher {
    public static final Pattern EMAIL_VALID_PATTERN = Pattern.compile(
            "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
    );

    private boolean isValid = false;

    public boolean isValid() {
        return this.isValid;
    }

    public static boolean isValidEmail(CharSequence email) {
        return email != null && EMAIL_VALID_PATTERN.matcher(email).matches();
    }

    @Override
    final public void afterTextChanged(Editable editable) {
        this.isValid = isValidEmail(editable);
    }

    @Override
    final public void beforeTextChanged(CharSequence sequence, int start, int count, int after) {

    }

    @Override
    final public void onTextChanged(CharSequence sequence, int start, int before, int count) {

    }

}
