package com.tktpl.helloworld.tutorial2.sharedPreferences;

import java.util.Calendar;

public class SharedPreferenceEntry {
    private final String mName;
    private final Calendar mDob;
    private final String mEmail;

    public SharedPreferenceEntry(String name, Calendar dob, String email) {
        mName = name;
        mDob = dob;
        mEmail = email;
    }

    public String getName() {
        return mName;
    }

    public Calendar getDob() {
        return mDob;
    }

    public String getEmail() {
        return mEmail;
    }
}
