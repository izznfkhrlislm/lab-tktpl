package com.tktpl.helloworld.tutorial2;

import com.tktpl.helloworld.R;
import com.tktpl.helloworld.tutorial2.sharedPreferences.SharedPreferenceEntry;
import com.tktpl.helloworld.tutorial2.sharedPreferences.SharedPreferencesHelper;
import com.tktpl.helloworld.tutorial2.validator.EmailValidator;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

public class Tutorial2Activity extends AppCompatActivity {
    private static final String TAG = "Tutorial2Activity";
    private SharedPreferencesHelper mSharedPreferencesHelper;
    private EditText mNameText;
    private DatePicker mDatePicker;
    private EditText mEmailText;
    private EmailValidator mEmailValidator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_tutorial2);

       mNameText = (EditText) findViewById(R.id.userNameInput);
       mDatePicker = (DatePicker) findViewById(R.id.dobInput);
       mEmailText = (EditText) findViewById(R.id.emailInput);

       mEmailValidator = new EmailValidator();
       mEmailText.addTextChangedListener(mEmailValidator);

       SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
       mSharedPreferencesHelper = new SharedPreferencesHelper(sharedPreferences);

       populateUiView();
    }

    private void populateUiView() {
        SharedPreferenceEntry sharedPreferenceEntry;
        sharedPreferenceEntry = mSharedPreferencesHelper.getUserInfo();

        mNameText.setText(sharedPreferenceEntry.getName());
        Calendar dob = sharedPreferenceEntry.getDob();
        mDatePicker.init(
            dob.get(Calendar.YEAR),
            dob.get(Calendar.MONTH),
            dob.get(Calendar.DAY_OF_MONTH),
null
        );
        mEmailText.setText(sharedPreferenceEntry.getEmail());
    }

    public void onSaveClick(View view) {
        Log.i(TAG, "name: " + mNameText.getText().toString() + "\nemail: " + mEmailText.getText().toString());
        if (!mEmailValidator.isValid()) {
            mEmailText.setError("Invalid email!");
            Log.w(TAG, "Data not saved: Invalid email!");
            return;
        }

        String name = mNameText.getText().toString();
        Calendar dob = Calendar.getInstance();
        dob.set(
            mDatePicker.getYear(),
            mDatePicker.getMonth(),
            mDatePicker.getDayOfMonth()
        );
        String email = mEmailText.getText().toString();

        SharedPreferenceEntry sharedPreferenceEntry = new SharedPreferenceEntry(
            name, dob, email
        );

        boolean isSuccess = mSharedPreferencesHelper.savePersonalInfo(sharedPreferenceEntry);
        if (isSuccess) {
            Toast.makeText(this, "Personal info saved!", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Success: Personal Info Saved");
        } else {
            Log.e(TAG, "Error: Personal Info Failed to Save");
        }
    }

    public void onRevertClick(View view) {
        boolean isSuccess = mSharedPreferencesHelper.removePersonalInfo();
        if (isSuccess) {
            Toast.makeText(this, "Personal info cleared!", Toast.LENGTH_LONG).show();
            populateUiView();
            Log.i(TAG, "Success: Personal Info Cleared");
            Log.i(TAG, "name: " + mNameText.getText().toString() + "\nemail: " + mEmailText.getText().toString());
        } else {
            Toast.makeText(this, "Failed to clear personal info!", Toast.LENGTH_SHORT).show();
        }
    }

}
