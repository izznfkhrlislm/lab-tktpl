package com.tktpl.helloworld.tutorial3;

import com.tktpl.helloworld.R;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Tutorial3Activity extends AppCompatActivity {
    private static final String TAG = "Tutorial3Activity";
    private TextView timeTextView;
    private Handler mainHandler = new Handler();
    private int second = 3;
    private boolean isFinished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial3);
        timeTextView = findViewById(R.id.text_view_countdown);
        timeTextView.setText(Integer.toString(second));
    }

    public void startTimer(View view) {
        isFinished = false;
        timer();
    }

    public void stopTimer(View view) {
        isFinished = true;
    }

    private void timer() {
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                if (second < 0) {
                    isFinished = true;
                    timeTextView.setText(Integer.toString(3));
                }

                if (!isFinished) {
                    second--;
                    timeTextView.setText(Integer.toString(second));
                    mainHandler.postDelayed(this, 1000);
                }
            }
        });
    }


}
