package com.tktpl.helloworld;

import com.tktpl.helloworld.tutorial2.validator.EmailValidator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EmailValidatorTest {

    @Test
    public void emailValidator_CorrectEmailSimple_ReturnsTrue() {
        assertTrue(EmailValidator.isValidEmail("izzan.fakhril@gmail.com"));
    }

    @Test
    public void emailValidator_CorrectEmailSubdomain_ReturnsTrue() {
        assertTrue(EmailValidator.isValidEmail("izzan.fakhril@ui.ac.id"));
    }

    @Test
    public void emailValidator_InvalidEmailNoDomain_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail("haha@hehe"));
    }

    @Test
    public void emailValidator_EmptyEmailString_ReturnsFalse() {
        assertFalse(EmailValidator.isValidEmail(""));
    }
}
