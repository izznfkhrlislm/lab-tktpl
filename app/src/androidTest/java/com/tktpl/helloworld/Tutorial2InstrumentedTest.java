package com.tktpl.helloworld;

import android.content.Context;
import android.view.View;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.tktpl.helloworld.tutorial2.Tutorial2Activity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class Tutorial2InstrumentedTest {
    @Rule
    public ActivityTestRule<Tutorial2Activity> rule = new ActivityTestRule<>(Tutorial2Activity.class);

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.tktpl.helloworld", appContext.getPackageName());
    }

    @Test
    public void ensureMainActivityIsPresent() throws Exception {
        Tutorial2Activity tutorial2Activity = rule.getActivity();

        View usernameInputTextfield = tutorial2Activity.findViewById(R.id.userNameInput);
        assertNotNull(usernameInputTextfield);

        View emailInputTextfield = tutorial2Activity.findViewById(R.id.emailInput);
        assertNotNull(emailInputTextfield);

        View dateOfBirthDatePicker = tutorial2Activity.findViewById(R.id.dobInput);
        assertNotNull(dateOfBirthDatePicker);
    }
}
